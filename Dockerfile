FROM python:3.10-slim as builder

SHELL ["/bin/bash", "-c"]

RUN apt update -y && \
    apt install -y --no-install-recommends \
    python-dev \
    build-essential && \
    rm -rf /var/cache/apt/* /var/lib/apt/lists/* && \
    useradd -Um -u 1000 --shell /bin/bash user

USER user
WORKDIR /project/
ENV PATH="~/.local/bin:$PATH"

COPY ./poetry.lock .
COPY ./pyproject.toml .

RUN python3 -c 'from urllib.request import urlopen; print(urlopen("https://install.python-poetry.org").read().decode())' | \
    python3 - --version 1.4.2 && \
    poetry install --no-cache


FROM python:3.10-slim as app

EXPOSE 8000

SHELL ["/bin/bash", "-c"]

RUN useradd -Um -u 1000 --shell /bin/bash user && \
    apt update -y && \
    apt install -y --no-install-recommends imagemagick && \
    rm -rf /var/cache/apt/* /var/lib/apt/lists/*

USER user
WORKDIR /project/
ENV PATH="~/.local/bin:$PATH"

RUN mkdir /tmp/uploads/

COPY --link --from=builder --chown=user:user /home/user/.local/ /home/user/.local/
COPY --link --from=builder --chown=user:user /home/user/.cache/pypoetry/ /home/user/.cache/pypoetry/
COPY --link --from=builder --chown=user:user /project/poetry.lock /project/poetry.lock
COPY --link --from=builder --chown=user:user /project/pyproject.toml /project/pyproject.toml
COPY ./src/webHandleFiles/ /project/

ENTRYPOINT poetry run uwsgi --py-auto-reload 2 --http :8000 --master -p 4 -w app:app
