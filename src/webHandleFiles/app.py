import os

from flask import Flask, request, send_file, url_for, make_response
from pydantic import BaseModel
from hashlib import md5
from pathlib import Path
from logging import DEBUG


app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = Path("/tmp/uploads/")
app.logger.setLevel(DEBUG)
app.logger.info("Init.")

FILE_SUFFIX = "processed.jpg"


class ResponseSchema(BaseModel):
    data: dict | BaseModel
    actions: list[str]
    errors: list[dict] = None


def salt(filename) -> str:
    return filename[:3] + filename


def image_resize(uploaded_file: Path):
    app.logger.debug(f"Start processing file <{uploaded_file}>.")
    # use sleep in shell to emulate long process
    os.popen(
        f"sleep 5 && convert {uploaded_file} -resize 100x {uploaded_file}-{FILE_SUFFIX}"
    )
    app.logger.debug(f"Result saved in <{uploaded_file}-{FILE_SUFFIX}>")


@app.route("/")
def index():
    app.logger.info("Hello!")
    response = ResponseSchema(
        data={"message": "Hello!"},
        actions=[
            url_for("index"),
            url_for("resize_image"),
        ],
    )
    return response.dict()


@app.route("/resize/", methods=["POST"])
def resize_image():
    if not request.files:
        response_content = ResponseSchema(
            data={},
            actions=[],
            errors=[{"message": "There is no files to resize"}]
        )
        return response_content.dict(), 406

    app.logger.info(f"<{request}>, <{request.files}>")

    uploaded_file = request.files["image"]
    hashed_name = md5(salt(uploaded_file.filename).encode("utf8")).hexdigest()
    file_path = app.config["UPLOAD_FOLDER"] / hashed_name
    uploaded_file.save(file_path)

    image_resize(file_path)

    response = ResponseSchema(
        data={"message": "File uploaded successfully and processing starts."},
        actions=[f"/receive/{hashed_name}"],
    )
    return response.dict()


@app.route("/receive/<string:hashed_name>")
def receive_image(hashed_name):
    file: Path = app.config["UPLOAD_FOLDER"] / f"{hashed_name}-{FILE_SUFFIX}"
    app.logger.info(f"Requested file <{file}>.")
    if not file.exists():
        app.logger.info("File not found.")
        response_content = ResponseSchema(
            data={}, actions=[], errors=[{"message": "File not found."}]
        )
        return response_content.dict(), 204

    return send_file(file, download_name=FILE_SUFFIX, as_attachment=True)
