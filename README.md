## Build
```shell
docker buildx build --tag webhandlefiles .
```

## Run
```shell
docker run --rm -p 127.0.0.1:8000:8000 webhandlefiles
```

## Usage
```shell
# Send image
curl -XPOST http://localhost:8000/resize/ -F "image=@/path/to/your/image.jpg"
# Returns:
#    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
#                                 Dload  Upload   Total   Spent    Left  Speed
#100 58244  100   143  100 58101    141  57355  0:00:01  0:00:01 --:--:-- 57553
#{
#  "actions": [
#    "/receive/0a22540832efa15801d14eca95df0109"
#  ],
#  "data": {
#    "message": "File uploaded successfully and processing starts."
#  },
#  "errors": null
#}

# Get resized image
wget http://localhost:8000/receive/0a22540832efa15801d14eca95df0109
# Returns 204 if still processing
#--2023-05-18 02:21:07--  http://localhost:8000/receive/0a22540832efa15801d14eca95df0109
#Resolving localhost (localhost)... 127.0.0.1
#Connecting to localhost (localhost)|127.0.0.1|:8000... connected.
#HTTP request sent, awaiting response... 204 NO CONTENT
#2023-05-18 02:21:07 (0,00 B/s) - ‘0a22540832efa15801d14eca95df0109’ saved [0]
```